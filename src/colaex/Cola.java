package colaex;

/**
 *
 * @author Melina Karen Ticra Aguilar 
 * CI: 15208024
 */
public class Cola <T> {
    private nodo<T>primero;
    private nodo<T>ultimo;
    private int size;
    private int limit;
    
    public Cola(){
        this.primero=null;
        this.ultimo=null;
        this.size=0;
        this.limit=-1;
    }
    public boolean isEmpty(){
     return primero==null;
    }
    public int sizeCola(){
        return size;
    }
    public void setLimit(int limit){
        this.limit=limit;//cambia el limit
    }
    public int getLimit(){
        return this.limit;
    }
    public boolean isLimit(){
        return size==limit;
    }
    public T firstElement(){
        if(isEmpty()){
            return null;
        }
        return primero.getValor();
    }

    public void enQueue(T element){
        nodo<T> newElem=new nodo(element,null);
        if(isEmpty()){
            primero=newElem;
            ultimo=newElem;
        }else{
            if(sizeCola()==1){
                primero.setSigNod(newElem);
            }else{
                ultimo.setSigNod(newElem);
            }
            ultimo=newElem;
        }
    }
    public T deQueue(){
        if(isEmpty()){
            return null;
        }else{
            T element=primero.getValor();
            nodo<T> aux=primero.getSigNod();
            primero=aux;
            size--;
            if(isEmpty()){
                ultimo=null;
            }
            return element;
        }
    }
}

