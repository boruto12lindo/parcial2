
package colaex;
/**
 * @author Melina Karen Ticra Aguilar 
 * CI: 15208024
 */
public class operaciones<T> {
    Cola<T> po;
    int size;
    public operaciones(Cola p, int size){
        this.po=p;
        this.size=size;
    }
     public void insert(T element){
        po.enQueue(element);
    }
    public void remove(){
        System.out.println("elemento "+po.deQueue()+" fue eliminado");
    }  
    public void occupiedFree(){
        System.out.println("Elementos existentes: "+po.firstElement());
    }
    public void maxMin(){
        Cola<T> paux1 = new Cola<>();
        int min = Integer.MIN_VALUE;
        int max = Integer.MAX_VALUE;
        String resultmin = null;
        String resultmax = null;
        while(!po.isEmpty()){
            String temp = String.valueOf(po.firstElement());
            char character = temp.charAt(0);
            int ascii = (int) character;
            if(ascii <= max){
                resultmin = temp;
                max = ascii;
            }
            if(ascii >= min){
                resultmax = temp;
                min = ascii;
            }
            paux1.enQueue(po.deQueue());
        }
        while(!paux1.isEmpty()){
            po.enQueue(paux1.deQueue());
        }
        System.out.println("Minimo: "+resultmin+" - Maximo: "+resultmax);
    }
     public void searchCola(String search){
        Cola<T> caux1 = new Cola<>();
        boolean exist = false;
        String temp = null;
        while(!po.isEmpty()){
            temp = String.valueOf(po.firstElement());
            if(temp.equals(search)){
                exist = true;
            }
            caux1.enQueue(po.deQueue());
        }
        while(!caux1.isEmpty()){
            po.enQueue(caux1.deQueue());
        }
        
        if(exist){
            System.out.println("elemento "+ search +" fue encontrado");
        }else{
            System.out.println("elemento "+ search +" no encontrado");
        }
    }
     public void sortStack(){
        Cola<T> tc = new Cola<>();
        Cola<T> tci = new Cola<>();
        while(!po.isEmpty()){
            tci.enQueue(po.deQueue());
            int currentData = (int) (String.valueOf(tci.firstElement())).charAt(0);
            int tcfirstEle = (int) (String.valueOf(tc.firstElement())).charAt(0);
            while(!tc.isEmpty() && tcfirstEle < currentData){
                po.enQueue(tc.deQueue());
            }
            tc.enQueue(tci.deQueue());
        }
        
        po = tc;
    }



}
